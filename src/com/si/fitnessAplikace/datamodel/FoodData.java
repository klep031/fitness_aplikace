package com.si.fitnessAplikace.datamodel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

public class FoodData {
    private static FoodData instance = new FoodData();
    private static String filename = "FoodListItems.txt";

    private ObservableList<FoodItem> foodItems;
    private DateTimeFormatter formatter;

    public static FoodData getInstance() {
        return instance;
    }

    private FoodData() {
        formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    public ObservableList<FoodItem> getFoodItems() {
        return foodItems;
    }

    public void addFoodItem(FoodItem item) {
        foodItems.add(item);
    }

    public void loadFoodItems() throws IOException {

        foodItems = FXCollections.observableArrayList();
        Path path = Paths.get(filename);
        BufferedReader br = Files.newBufferedReader(path);

        String input;

        try {
            while ((input = br.readLine()) != null) {
                String[] itemPieces = input.split("\t");

                String shortDescription = itemPieces[0];
                int Kcal = Integer.parseInt(itemPieces[1]);
                int protein = Integer.parseInt(itemPieces[3]);
                int carbs = Integer.parseInt(itemPieces[2]);
                int fat = Integer.parseInt(itemPieces[4]);
                FoodItem foodItem = new FoodItem(shortDescription, Kcal,carbs, protein, fat);
                foodItems.add(foodItem);
            }

        } finally {
            if(br != null) {
                br.close();
            }
        }
    }

    public void storeTodoItems() throws IOException {

        Path path = Paths.get(filename);
        BufferedWriter bw = Files.newBufferedWriter(path);
        try {
            Iterator<FoodItem> iter = foodItems.iterator();
            while(iter.hasNext()) {
                FoodItem item = iter.next();
                bw.write(String.format("%s\t%d\t%d\t%d\t%d",
                        item.getShortDescription(),
                        item.getKcal(),
                        item.getBilkoviny(),
                        item.getSacharidy(),
                        item.getTuky()));
                bw.newLine();
            }

        } finally {
            if(bw != null) {
                bw.close();
            }
        }
    }

}
