package com.si.fitnessAplikace.datamodel;

import java.time.LocalDate;

public class FoodItem {
    private int sacharidy;
    private int tuky;
    private int bilkoviny;
    private int kcal;
    private String shortDescription;

    public FoodItem(String shortDescription, int kcal, int carbs, int protein, int fat) {
        this.shortDescription = shortDescription;
        this.kcal = kcal;
        this.bilkoviny = protein;
        this.sacharidy = carbs;
        this.tuky = fat;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }


    public int getSacharidy() {
        return sacharidy;
    }

    public int getTuky() {
        return tuky;
    }

    public int getKcal() {
        return kcal;
    }

    public int getBilkoviny() {
        return bilkoviny;
    }

    public void setSacharidy(int sacharidy) {
        this.sacharidy = sacharidy;
    }

    public void setTuky(int tuky) {
        this.tuky = tuky;
    }

    public void setBilkoviny(int bilkoviny) {
        this.bilkoviny = bilkoviny;
    }

    public void setKcal(int kcal) {
        this.kcal = kcal;
    }
}
