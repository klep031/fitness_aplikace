package com.si.fitnessAplikace.datamodel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;

public class StatsData {
    private HashMap<String, Integer> stats;
    private static StatsData instance = new StatsData();
    private static String filename = "stats.txt";

    public static StatsData getInstance() {
        return instance;
    }
    private StatsData() {
        stats = new HashMap<>();
    }

    public HashMap<String, Integer> getStats() {
        return stats;
    }

    public void loadStats() throws IOException {
        Path path = Paths.get(filename);
        BufferedReader br = Files.newBufferedReader(path);
        String input;
        try {
            while ((input = br.readLine()) != null) {
                String[] itemPieces = input.split("\t");
                stats.put(itemPieces[0], Integer.parseInt(itemPieces[1]));
            }

        } finally {
            if(br != null) {
                br.close();
            }
        }

    }

    public void storeStats() throws IOException {

        Path path = Paths.get(filename);
        PrintWriter writer = new PrintWriter(filename);
        writer.print("");
        writer.close();
        BufferedWriter bw = Files.newBufferedWriter(path);
        try {
            for(String s : stats.keySet()) {
                bw.write(String.format("%s\t%d", s, stats.get(s)));
                bw.newLine();
            }
        } finally {
            if(bw != null) {
                bw.close();
            }
        }
    }
}
