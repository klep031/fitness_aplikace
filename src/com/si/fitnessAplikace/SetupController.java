package com.si.fitnessAplikace;

import com.si.fitnessAplikace.datamodel.StatsData;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.HashMap;

public class SetupController {
    @FXML
    private Label kcalLabel;
    @FXML
    private Label fatLabel;
    @FXML
    private Label proteinLabel;
    @FXML
    private Label carbsLabel;
    @FXML
    private TextField kcalGoal;
    @FXML
    private TextField proteinGoal;
    @FXML
    private TextField fatGoal;
    @FXML
    private TextField carbsGoal;
    public HashMap<String, Integer> processResults() {
        HashMap<String, Integer> res = StatsData.getInstance().getStats();
        //HashMap<String, Integer> res = new HashMap<>();

        int kcal = Integer.parseInt(kcalGoal.getText());
        int protein = Integer.parseInt(proteinGoal.getText());
        int fat = Integer.parseInt(fatGoal.getText());
        int carbs = Integer.parseInt(carbsGoal.getText());
        res.put(kcalLabel.getText(), kcal);
        res.put(proteinLabel.getText(),protein);
        res.put(fatLabel.getText(),fat);
        res.put(carbsLabel.getText(),carbs);
        return res;
    }
}
