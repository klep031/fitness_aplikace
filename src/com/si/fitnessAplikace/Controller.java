package com.si.fitnessAplikace;

import com.si.fitnessAplikace.datamodel.FoodData;

import com.si.fitnessAplikace.datamodel.FoodItem;
import com.si.fitnessAplikace.datamodel.StatsData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class Controller {

    private List<FoodItem> todoItems;
    private HashMap<String, Integer> stats;

    @FXML
    private ListView<FoodItem> foodListView;

    @FXML
    private TextArea itemDetailsTextArea;

    @FXML
    private Label deadlineLabel;

    @FXML
    private Label kcalLabel;

    @FXML
    private Label proteinLabel;

    @FXML
    private Label fatLabel;

    @FXML
    private Label carbsLabel;

    @FXML
    private BorderPane mainBorderPane;

    public void initialize() {

        foodListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<FoodItem>() {
            @Override
            public void changed(ObservableValue<? extends FoodItem> observable, FoodItem oldValue, FoodItem newValue) {
                if(newValue != null) {
                    FoodItem item = foodListView.getSelectionModel().getSelectedItem();
                    kcalLabel.setText(String.valueOf(item.getKcal()));
                    proteinLabel.setText(String.valueOf(item.getBilkoviny()));
                    fatLabel.setText(String.valueOf(item.getTuky()));
                    carbsLabel.setText(String.valueOf(item.getSacharidy()));
//                    DateTimeFormatter df = DateTimeFormatter.ofPattern("MMMM d, yyyy"); // "d M yy");
//                    deadlineLabel.setText(df.format(item.getDeadline()));
                }
            }
        });

        foodListView.setItems(FoodData.getInstance().getFoodItems());
        foodListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        foodListView.getSelectionModel().selectFirst();
        stats = StatsData.getInstance().getStats();

        foodListView.setCellFactory(new Callback<ListView<FoodItem>, ListCell<FoodItem>>() {
            @Override
            public ListCell<FoodItem> call(ListView<FoodItem> param) {
                ListCell<FoodItem> cell = new ListCell<FoodItem>() {

                    @Override
                    protected void updateItem(FoodItem item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty) {
                            setText(null);
                        }
                        else {
                            setText(item.getShortDescription());
                        }
//                            if(item.getDeadline().isBefore(LocalDate.now().plusDays(1))) {
//                                setTextFill(Color.RED);
//                            } else if(item.getDeadline().equals(LocalDate.now().plusDays(1))) {
//                                setTextFill(Color.BROWN);
//                            }
//                        }
                    }
                };

                return cell;
            }
        });
    }

    @FXML
    public void showNewItemDialog() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setTitle("Add New food");
        dialog.setHeaderText("Use this dialog to create a new food item");
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("todoItemDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(fxmlLoader.load());

        } catch(IOException e) {
            System.out.println("Couldn't load the dialog");
            e.printStackTrace();
            return;
        }

        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

        Optional<ButtonType> result = dialog.showAndWait();
        if(result.isPresent() && result.get() == ButtonType.OK) {
            DialogController controller = fxmlLoader.getController();
            FoodItem newItem = controller.processResults();
            foodListView.getSelectionModel().select(newItem);
        }


    }

    public void showOverview() {
        int currentKcal = foodListView.getItems().stream().map(FoodItem::getKcal)
                .reduce(0, Integer::sum);

        int currentFats = foodListView.getItems().stream().map(FoodItem::getTuky)
                .reduce(0, Integer::sum);

        int currentProteins = foodListView.getItems().stream().map(FoodItem::getBilkoviny)
                .reduce(0, Integer::sum);

        int currentCarbs = foodListView.getItems().stream().map(FoodItem::getSacharidy)
                .reduce(0, Integer::sum);
        double KcalRatio = currentKcal/ (double)stats.get("Kcal");
        double fatRation = currentFats/ (double)stats.get("Fat");
        double proteinRation = currentProteins/stats.get("Protein");
        double carbsRation = currentCarbs/stats.get("Carbs");
        System.out.println(carbsRation);
        Dialog<ButtonType> overviewDialog = new Dialog<>();
        overviewDialog.initOwner(mainBorderPane.getScene().getWindow());
        overviewDialog.setTitle("Watch your current nutrients");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("overviewDialog.fxml"));
        try {
            overviewDialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        overviewDialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        overviewDialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        OverviewController controller = loader.getController();
        controller.udpateOverview(stats.get("Kcal"),stats.get("Protein"),stats.get("Fat"),stats.get("Carbs"),
                currentKcal,currentProteins,currentFats,currentCarbs);
        Optional<ButtonType> res = overviewDialog.showAndWait();
    }

    public void setGoals() {
        Dialog<ButtonType> setupDialog = new Dialog<>();
        setupDialog.initOwner(mainBorderPane.getScene().getWindow());
        setupDialog.setTitle("Custom your Goals");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("setupDialog.fxml"));
        try {
            setupDialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        setupDialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        setupDialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> res  = setupDialog.showAndWait();
        if(res.isPresent() && res.get() == ButtonType.OK) {
            SetupController controller = loader.getController();
            stats = controller.processResults();
            for(String s : stats.keySet()) {
                System.out.println(s);
                System.out.println(stats.get(s));
            }
        }
    }

    @FXML
    public void handleClickListView() {
        FoodItem item = foodListView.getSelectionModel().getSelectedItem();
//        itemDetailsTextArea.setText(item.getDetails());
//        deadlineLabel.setText(item.getDeadline().toString());
    }
    public void countCurrentKcal() {
        AtomicInteger currentKcal = new AtomicInteger();
        int kcal = foodListView.getItems().stream().map(FoodItem::getKcal)
                .reduce(0, Integer::sum);
    }
}
