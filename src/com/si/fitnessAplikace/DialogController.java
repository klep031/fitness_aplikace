package com.si.fitnessAplikace;

import com.si.fitnessAplikace.datamodel.FoodData;
import com.si.fitnessAplikace.datamodel.FoodItem;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.time.LocalDate;

public class DialogController {

    @FXML
    private TextField shortDescriptionField;

    @FXML
    private TextField kcalField;

    @FXML
    private TextField proteinField;

    @FXML
    private TextField fatField;

    @FXML
    private TextField carbField;


    @FXML
    private DatePicker deadlinePicker;
    public void test() {

    }

    public FoodItem processResults() {
        String shortDescription = shortDescriptionField.getText().trim();
        LocalDate deadlineValue = deadlinePicker.getValue();
        int kcal = Integer.parseInt(kcalField.getText());
        int protein = Integer.parseInt(proteinField.getText());
        int fat = Integer.parseInt(fatField.getText());
        int carbs = Integer.parseInt(carbField.getText());

        FoodItem newItem = new FoodItem(shortDescription, kcal, carbs,protein,fat);
        FoodData.getInstance().addFoodItem(newItem);
        return newItem;
    }
}
