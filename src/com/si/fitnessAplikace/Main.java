package com.si.fitnessAplikace;

import com.si.fitnessAplikace.datamodel.FoodData;
import com.si.fitnessAplikace.datamodel.StatsData;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("mainwindow.fxml"));
        primaryStage.setTitle("Fitness aplikace");
        primaryStage.setScene(new Scene(root, 600, 500));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        try {
            FoodData.getInstance().storeTodoItems();
            StatsData.getInstance().storeStats();

        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void init() throws Exception {
        try {
            FoodData.getInstance().loadFoodItems();
            StatsData.getInstance().loadStats();

        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
