package com.si.fitnessAplikace;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

public class OverviewController {
    @FXML
    private Label overviewKcal;
    @FXML
    private Label overviewProtein;
    @FXML
    private Label overviewFat;
    @FXML
    private Label overviewCarbs;
    @FXML
    private ProgressBar kcalBar;
    @FXML
    private ProgressBar proteinBar;
    @FXML
    private ProgressBar fatBar;
    @FXML
    private ProgressBar carbsBar;

    public void udpateOverview(int goalKcal, int goalProtein, int goalFat, int goalCarbs,
                               int kcal, int protein, int fat, int carbs) {

        double KcalRatio = kcal/ (double)goalKcal;
        double fatRatio = fat/ (double)goalFat;
        double proteinRatio = protein/(double)goalProtein;
        double carbsRatio = carbs/(double)goalCarbs;
        proteinBar.setProgress(proteinRatio);
        kcalBar.setProgress(KcalRatio);
        carbsBar.setProgress(carbsRatio);
        fatBar.setProgress(fatRatio);
        overviewKcal.setText(kcal + "/" + goalKcal);
        overviewCarbs.setText(carbs + "/" + goalCarbs);
        overviewFat.setText(fat + "/" + goalFat);
        overviewProtein.setText(protein + "/" + goalProtein);
    }

}
